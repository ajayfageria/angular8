import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './account/login/login.component';
import { TouristsComponent } from './tourists/tourists.component';
import { LogohomeComponent } from './logohome/logohome.component';
import { ActivitiesComponent } from './activities/activities.component';
import { FoodComponent } from './food/food.component';
import { AccommodationComponent } from './accommodation/accommodation.component';
import { BlogsComponent } from './blogs/blogs.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { SignupComponent } from './account/signup/signup.component';


const routes: Routes = [
  { path: '', redirectTo: '/logohome', pathMatch: 'full' },
  {path:'logohome',component:LogohomeComponent},
  {path:'home',component:LogohomeComponent},
  {path:'tourists',component:TouristsComponent},
 
  {path:'activities',component:ActivitiesComponent},
  
  {path:'accommodation',component:AccommodationComponent},
  {path:'food',component:FoodComponent},
  {path:'blogs',component:BlogsComponent},
  {path:'about-us',component:AboutUsComponent},
  {path:'signup',component:SignupComponent},
  {path:'login',component:LoginComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
